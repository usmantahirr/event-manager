﻿module.exports = {

  identity: 'g',
  
	connection: 'mysqlDB',
	schema:true,
	migrate: 'alter',

  attributes: {
	g_id: 'int', 
    g_name: 'string',
    g_catagory: 'string',
	//one to many
  videos: {
  collection: 'video',
  via: 'users'
  },
  images: {
  collection: 'image',
  via: 'user'
  }
  
   }
};

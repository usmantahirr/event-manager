﻿module.exports = {
    identity: 'events',

    connection: 'mysqlDB',
    schema:true,
    migrate: 'alter',

    attributes: {
        events_date: 'date',
        events_title: 'string',
        description: 'string'
    }
};

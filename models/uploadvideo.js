module.exports = {
	identity: 'uploadvideo',
	
	connection: 'mysqlDB',
	schema:true,
	migrate: 'alter',
	
	attributes: {
		video: 'string',
		name: 'string'
	}
};

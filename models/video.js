﻿module.exports = {
	identity: 'video',
	
	connection: 'mysqlDB',
	schema:true,
	migrate: 'alter',
	
	attributes: {
		text: 'string',
		title:'string',
		order: 'string',
		vid: 'string',
		//one to many
	users: {
	columnName: 'gallery_id',
	model: 'g'
	}
	
	}
};

﻿module.exports = {
    identity: 'image',

    connection: 'mysqlDB',
    schema:true,
    migrate: 'alter',

    attributes: {
        img: 'string',
        img_title:'string',
        img_des: 'string',
        img_order: 'string',
//one to many

        user: {
            columnName: 'g_id',
            model: 'g'


        }
    }
};

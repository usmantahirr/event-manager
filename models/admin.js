﻿module.exports = {
    identity: 'admin',

    connection: 'mysqlDB',
    schema:true,
    migrate: 'alter',

    attributes: {
        userName: 'string',
        email:'string',
        password: 'string'
    }
};

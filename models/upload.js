module.exports = {
	identity: 'upload',
	
	connection: 'mysqlDB',
	schema:true,
	migrate: 'alter',
	
	attributes: {
		image: 'string',
		name: 'string'
	}
};

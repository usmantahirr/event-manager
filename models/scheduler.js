module.exports = {
    identity: 'scheduler',

    connection: 'mysqlDB',
    schema:true,
    migrate: 'alter',

    attributes: {
        id: 'int',
        text:'string',
        start_date: 'string',
        end_date: 'string'
    }
};

﻿module.exports = {
    identity: 'crud',

    connection: 'mysqlDB',
    schema:true,
    migrate: 'alter',

    attributes: {
        news_date: 'date',
        news_title: 'string',
        description: 'string',
        news_day: 'string',
        news_month: 'string',
        news_year: 'int'
    }
};

//loading view file
exports.index = function(req, res) {
    res.sendfile("views/admin/index.html");
};

//returning all admin
exports.new = function (req,res) {
    db('admin').find().done(function (err, models) {
        if (err)
            console.log("Error in Index of MYSQL:"+err);
        else
            res.json(models);
    });
};

//creating admin
exports.create = function (req, res) {
    var admin=req.body;
    db('admin').create(admin, function (err, model) {
        if (err)
            console.log("Error in Create of MYSQL:"+err);
        else
            res.redirect('/');
    });
};

//returning data of single admin
exports.show = function (req, res) {
    if (req.params.id != "favicon") {
        db('admin').findOne({ id: req.params.admin }).done(function (err, model) {
            if ( err )
                console.log("Error in Show of MYSQL"+err);
            else
                res.json(model);
        });
    }
};

//updating admin data
exports.update = function (req, res) {
    var id=req.params.admin;
    var userName = req.body.userName;
    var email = req.body.email;
    var password = req.body.password;

    db('admin').findOne({ id: id }).done(function (err, model) {
        model.userName = userName;
        model.email = email;
        model.password = password;
        console.dir(model);
        model.save(function (err) {
            if (err)
                console.log("Error in Index of MYSQL"+err);
            else
                res.redirect('/');
        });
    });
};

//deleting admin
exports.destroy = function(req, res) {
    var id = req.params.admin;
    db('admin').findOne({ id: id }, function (err, model) {
        if (model) {
            db('admin').destroy({ id: id }, function (err) {
                if (err)
                    console.log("Error in Index of MYSQL"+err);
                else
                    res.redirect('/');
            });
        } else {
            res.send('not found');
        }
    });
};
var SCHEDULER_TABLE = 'scheduler';

// Landing Page
exports.index = function(req, res){
    res.sendfile("views/Scheduler/index.html");
};

exports.new = function(req, res){
    res.send('New Part');
};

exports.init = function(req, res){
    db(SCHEDULER_TABLE).create(req.body, function(err, customer) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.send("success");
        }
    });
};

exports.saveEvent = function(req, res) {
    db(SCHEDULER_TABLE).create(req.body, function (err, customer) {
        if (err) {
            console.log(err);
            res.send(err);
        }
        else {
            res.send("success");
        }
    });
};

exports.getAllEvents = function(req, res) {
    db(SCHEDULER_TABLE).find().done(function (err, models) {
        if (err)
            console.log("Error in Index of MYSQL:"+err);
        else
            res.json(models);
    });
};
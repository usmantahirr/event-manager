exports.index = function(req, res){
	res.sendfile("views/index.html");
};

//creating user
exports.create = function (req, res) {
	console.log("Sign In - Create");
	db('admin').findOne({ email: req.body.email,password :req.body.password }).done(function (err, model) {
		if ( model ) {
			req.session.user = model;
			res.redirect("/admin");
		} else {
			res.send("user not found");
		}
	});
};

exports.user_signout=function(req,res) {
	req.session.user={};
	delete req.session;
	res.redirect("/");
};

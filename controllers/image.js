//loading view file
exports.index = function(req, res) {
    res.sendfile("views/image/index.html");
};

//returning all image
exports.new = function (req,res) {
    db('upload').find().done(function (err, models) {
        if ( err )
            console.log("Error in Index of MYSQL:"+err);
        else
            res.json(models );
    });
};

//creating image
exports.create=function (req, res){
    var image=req.body;
    db('image').create(image, function (err, model) {
        if (err)
            console.log("Error in Create of MYSQL:"+err);
        else
            res.json(model);
    });

};
exports.assign_view = function(req, res) {
    console.log("in AC");
    var obj={};
    db('g').find().done(function(err, model) {
        db('image').find().done(function(err, models) {
            obj.g = model;
            obj.image = models;
            res.json(obj);
        });
    });
};
exports.assign_image=function(req, res){
    console.log("here is hte image ");
    console.log(req.body);
    console.log(req.body.gallery);

    db('g').findOne({id: req.body.gallery}).exec(function (err, model){
        if(err){
        }
        console.log(model);
        model.images.add(req.body.image);
        model.save(function (err) {
            res.send("error during save the assign image to gallery");
        });
        res.redirect('/');

    });
    //res.send("helo Assign of ");
};
//returning data of single image
exports.show = function (req,res) {
    if (req.params.id != "favicon") {
        db('image').findOne({ id: req.params.image }).done(function (err, model) {
            if ( err )
                console.log("Error in Show of MYSQL"+err);
            else
                console.log(model);
                res.json(model);
        });
    }
};

//updating image data
exports.update=function (req,res){
    var id=req.params.image;
    var img = req.body.img;
    var img_title = req.body.img_title;
    var img_des = req.body.img_des;
    var img_order = req.body.img_order;

    db('image').findOne({ id: id }).done(function (err, model) {
        model.img = img;
        model.img_title = img_title;
        model.img_des = img_des;
        model.img_order = img_order;
        console.dir(model);
        model.save(function (err) {
            if (err)
                console.log("Error in Index of MYSQL"+err);
            else
                res.redirect('/');
        });
    });
};

//deleting image
exports.destroy=function(req, res){
    var id = req.params.image;
    db('image').findOne({ id: id }, function (err, model) {
        if (model) {
            db('image').destroy({ id: id }, function (err) {
                if (err)
                    console.log("Error in Index of MYSQL"+err);
                else
                    res.redirect('/');
            });
        } else {
            res.send('not found');
        }
    });
};
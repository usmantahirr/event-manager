//loading view file
exports.index = function(req, res) {
    res.sendfile("views/video/index.html");
};

//returning all video
exports.new = function (req, res) {
    db('uploadvideo').find().done(function (err, models) {
        if ( err )
            console.log("Error in Index of MYSQL:"+err);
        else
            res.json(models);
    });
};

//creating video
exports.create=function (req, res) {
    var video=req.body;

    db('video').create(video, function (err, model) {
        if (err)
            console.log("Error in Create of MYSQL:"+err);
        else
            res.redirect('/');
    });

};
exports.assign_view=function(req, res){
    db('g').find().done(function(err,model){
        db('video').find().done(function(err,sub){
            res.render("video/assign",{title: "Assigning video", video:sub , g:model});
        });
    });
};
exports.assign_video=function(req, res) {
    db('g').findOne({id: req.body.users}).exec(function (err, model) {
        if(err) {
        }
        model.videos.add(req.body.videos);
        model.save(function (err) {

            res.send("error during save the assign video to gallery");
        });
        res.redirect('/');
    });
    //res.send("helo Assign of Car");
};
exports.assign_view = function(req, res) {
    console.log("in AC");
    var obj = {};
    db('g').find().done(function(err, model) {

        db('video').find().done(function(err, models) {
            obj.g=model;
            obj.video=models;
            res.json(obj);
        });
    });
};
exports.assign_video=function(req, res){
    console.log("here is the video ");
    console.log(req.body);
    console.log(req.body.gallery);

    db('g').findOne({id: req.body.gallery}).exec(function (err, model){
        if(err){
        }
        console.log(model);
        model.videos.add(req.body.video);
        model.save(function (err){

            res.send("error during save the assign image to gallery");
        });
        res.redirect('/');

    });
    //res.send("helo Assign of ");
};
//returning data of single video
exports.show = function (req,res) {
    if (req.params.id != "favicon") {
        db('video').findOne({ id: req.params.video }).done(function (err, model) {
            if ( err )
                console.log("Error in Show of MYSQL" + err);
            else
                res.json(model);
        });
    }
};

//updating video data
exports.update=function (req,res){
    var id=req.params.video;
    var text = req.body.text;
    var title = req.body.title;
    var order = req.body.order;
    var vid = req.body.vid;

    db('video').findOne({ id: id }).done(function (err, model) {
        model.text = text;
        model.title = title;
        model.order = order;
        model.vid = vid;
        console.dir(model);
        model.save(function (err) {
            if (err)
                console.log("Error in Index of MYSQL"+err);
            else
                res.redirect('/');
        });
    });
};

//deleting video
exports.destroy=function(req, res){
    var id = req.params.video;
    db('video').findOne({ id: id }, function (err, model) {
        if (model) {
            db('video').destroy({ id: id }, function (err) {
                if (err)
                    console.log("Error in Index of MYSQL"+err);
                else
                    res.redirect('/');
            });
        } else {
            res.send('not found');
        }
    });
};
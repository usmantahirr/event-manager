//loading view file
exports.index = function(req, res){
	res.sendfile("views/crud/index.html");
};

//returning all news
exports.new=function (req,res){
    db('crud').find().done(function (err, models) {
		if ( err )
			console.log("Error in Index of MYSQL:"+err);
        else
            res.json(models );
    });
}

//creating news
exports.create=function (req, res){
	var news=req.body;
	
	db('crud').create(news, function (err, model) {
        if (err)
			console.log("Error in Create of MYSQL:"+err);
        else
			res.redirect('/');
    });
	
};

//returning data of single news
exports.show=function (req,res){
    if (req.params.id != "favicon") {
        db('crud').findOne({ id: req.params.crud }).done(function (err, model) {
            if ( err )
				console.log("Error in Show of MYSQL"+err);
            else
                res.json(model);
        });
    }
};

//updating news data
exports.update=function (req,res){
	var id=req.params.crud;
    var news_date = req.body.news_date;
    var news_title = req.body.news_title;
	var description = req.body.description;
	 var news_day = req.body.news_day;
    var news_month = req.body.news_month;
	var news_year = req.body.news_year;

    db('crud').findOne({ id: id }).done(function (err, model) {
        model.news_date = news_date;
        model.news_title = news_title;
		model.description = description;
		model.news_day = news_day;
        model.news_month = news_month
		
		;
		model.news_year = news_year;
		console.dir(model);
        model.save(function (err) {
            if (err) 
				console.log("Error in Index of MYSQL"+err);
            else
				res.redirect('/');
        });
    });
};
/*exports.archive=function (req,res){
    db('crud').find().sort(news_day) {
		.count(news_day)
        
            res.json(models );
    });
};
*/
//deleting news
exports.destroy=function(req, res){
    var id = req.params.crud;
    db('crud').findOne({ id: id }, function (err, model) {
        if (model) {
            db('crud').destroy({ id: id }, function (err) {
                if (err) 	
					console.log("Error in Index of MYSQL"+err);
                else
					res.redirect('/');
            });
        } else {
            res.send('not found');
        }
    });
};
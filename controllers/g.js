//loading view file
exports.index = function(req, res){
    res.sendfile("views/g/index.html");
};

//returning all users
exports.new=function (req,res){
    db('g').find().done(function (err, models) {
        if ( err )
            console.log("Error in Index of MYSQL:"+err);
        else
            res.json(models );
    });
};

//creating user
exports.create=function (req, res){
    var user=req.body;

    db('g').create(user, function (err, model) {
        if (err)
            console.log("Error in Create of MYSQL:"+err);
        else
            res.redirect('/');
    });

};

//returning data of single user
exports.show=function (req,res){
    if (req.params.id != "favicon") {
        db('g').findOne({ id: req.params.g }).done(function (err, model) {
            if ( err )
                console.log("Error in Show of MYSQL"+err);
            else
                console.log(model);
                res.json(model);

        });
    }
};

//updating user data
exports.update=function (req,res){
    var id=req.params.g;
    var g_name= req.body.g_name;
    var g_catagory= req.body.g_catagory;

    db('g').findOne({ id: id }).done(function (err, model) {
        model.g_name = g_name;
        model.g_catagory =g_catagory;

        console.dir(model);
        model.save(function (err) {
            if (err)
                console.log("Error in Index of MYSQL"+err);
            else
                res.redirect('/');
        });
    });
};

//deleting user
exports.destroy=function(req, res){
    var id = req.params.g;
    db('g').findOne({ id: id }, function (err, model) {
        if (model) {
            db('g').destroy({ id: id }, function (err) {
                if (err)
                    console.log("Error in Index of MYSQL"+err);
                else
                    res.redirect('/');
            });
        } else {
            res.send('not found');
        }
    });
};
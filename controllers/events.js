//loading view file
exports.index = function(req, res){
    res.sendfile("views/events/index.html");
};

//returning all news
exports.new = function (req,res) {
    db('events').find().done(function (err, models) {
        if ( err )
            console.log("Error in Index of MYSQL:"+err);
        else
            res.json(models );
    });
};

//creating news
exports.create = function (req, res) {
    var events = req.body;

    db('events').create(events, function (err, model) {
        if (err)
            console.log("Error in Create of MYSQL:"+err);
        else
            res.redirect('/');
    });

};

//returning data of single news
exports.show = function (req,res){
    if (req.params.id != "favicon") {
        db('events').findOne({ id: req.params.events }).done(function (err, model) {
            if ( err )
                console.log("Error in Show of MYSQL"+err);
            else
                res.json(model);
        });
    }
};

//updating news data
exports.update=function (req,res){
    var id=req.params.events;
    var events_date = req.body.events_date;
    var events_title = req.body.events_title;
    var description = req.body.description;

    db('events').findOne({ id: id }).done(function (err, model) {
        model.events_date = events_date;
        model.events_title = events_title;
        model.description = description;
        console.dir(model);
        model.save(function (err) {
            if (err)
                console.log("Error in Index of MYSQL"+err);
            else
                res.redirect('/');
        });
    });
};

//deleting news
exports.destroy=function(req, res){
    var id = req.params.events;
    db('events').findOne({ id: id }, function (err, model) {
        if (model) {
            db('events').destroy({ id: id }, function (err) {
                if (err)
                    console.log("Error in Index of MYSQL"+err);
                else
                    res.redirect('/');
            });
        } else {
            res.send('not found');
        }
    });
};
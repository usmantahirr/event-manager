// Resource Loading
var http = require('http');
var path = require('path');
var swig = require('swig');

// Middle wares
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var multer = require('multer');

// Loading Express
var express = require('express');

// Loading Express-Resource
var resource = require('express-resource');
var app = express();

// init Middle Wares
app.use(bodyParser());
app.use(methodOverride());
app.use(multer({
    dest: './views/uploads/'
}));

// Loading scheduler
var schedulerController = require('./controllers/scheduler.js'),
    schedulerResource = app.resource('scheduler', schedulerController);
// Mapping Scheduler links
schedulerResource.map('post', '/init', schedulerController.init);
schedulerResource.map('post', '/save', schedulerController.saveEvent);
schedulerResource.map('get', '/getSchedule', schedulerController.getAllEvents);

var schedulerEventController = require('./controllers/schedulerEvents.js'),
    schedulerEventResource = app.resource('eventview', schedulerEventController);

// Session Setup
var session = require('./node_modules/sesh/lib/core').magicSession();

// App Configurations
// Setting up view engine
app.engine('html', swig.renderFile);
app.set('view engine', 'html');

// telling it path of view folder
app.set('views', path.join(__dirname, 'views'));
app.set('view cache', false);

// setting template engine (SWIG)
swig.setDefaults({ cache: false });

// Setting up Upload Controller
var uploadController = require('./controllers/upload');
var uploadController1 = require('./controllers/uploadvideo');

app.get('/upload', uploadController.getUpload);
app.post('/upload', uploadController.postUpload);

app.get('/uploadvideo', uploadController1.getUpload);
app.post('/uploadvideo', uploadController1.postUpload);
app.set('view engine', 'jade');

// Setting up Image Controller
var img = require('./controllers/image.js');
var img1 = app.resource('image', img);
img1.map("get", "/get/assign_view", img.assign_view);
img1.map("post", "/post/assign_image", img.assign_image);
img1.map("get", "/show/user", img.show);

// Setting up Video Controller
var video = require('./controllers/video.js');
var video1 = app.resource('video', video);
video1.map("get", "/get/assign_view", video.assign_view);
video1.map("post", "/post/assign_video", video.assign_video);
video1.map("get", "/show/user", video.show);

// Setting Up Sign In Controller
var sign = require('./controllers/signinCtrl.js');
var signin2 = app.resource('signin', sign);
signin2.map('get', '/log/out', sign.user_signout);

// Setting up Routes/Resources
var galleryMapper = app.resource('g', require('./controllers/g.js'));
galleryMapper.map('get', '/show', galleryMapper.show);
app.resource('video', require('./controllers/video.js'));
app.resource('image', require('./controllers/image.js'));
app.resource('crud', require('./controllers/crud.js'));
app.resource(require('./controllers/default.js'));
app.resource('upload', require('./controllers/upload.js'));
app.resource('uploadvideo', require('./controllers/uploadvideo.js'));

// Admin
var admin = require('./controllers/admin.js');
var adminMapper = app.resource('admin', admin);
adminMapper.map('get', '/show', admin.show);

//App settings
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/views/scheduler'));

//ORM Configurations
var waterlineConfig = require('./config/waterline')
    , waterlineOrm = require('./init/models').waterlineOrm;

var modelPath = path.join(__dirname, '/models');
require('./init/models')(modelPath);

//ORM Initialization
waterlineOrm.initialize(waterlineConfig, function (err, models) {
    if (err) throw err;

    db = function (table) {
        return models['collections'][table];
    };
    db.connections = models.connections;

    http.createServer(app).listen(3000, function () {
        console.log('Express server listening on port ' + 3000);
    });
});
'use strict';
var firstApp=angular.module("firstApp",['ngRoute', 'ngResource'])
	.config(function ($routeProvider){
		var partialsDir = "crud/partials/news/";	
		$routeProvider
		.when('/',{
			templateUrl:'crud/partials/defaultPart.html'
		})
		.when('/news', {
			templateUrl: partialsDir+'guestPart.html', 
			controller: 'guestCtrl'
		});
		$routeProvider
		.otherwise({
			redirectTo:'/news'
		});
	});
'use strict';
var firstApp=angular.module("firstApp",['ngRoute', 'ngResource'])
	.config(function ($routeProvider){
		var partialsDir = "g/partials/g/";	
		$routeProvider
		.when('/',{
			templateUrl: 'g/partials/defaultPart.html'
		})
		.when('/g', {
			templateUrl: partialsDir+'gPart.html', 
			controller: 'usersCtrl'
		})
		.when('/new', {
			templateUrl: partialsDir+'gNewPart.html',
			controller: 'userNewCtrl'
		})
		.when('/:id', {
        templateUrl: partialsDir+'gShowPart.html',
     	controller: 'userShowCtrl'
		})
        .when('/:id/edit', {
            templateUrl: partialsDir+'gEditPart.html',
			controller: 'userEditCtrl'
        })
        .when('/:id/del', {
            template: '<h1>Deleted</h1>',
			controller: 'userDelCtrl'
        });
		$routeProvider
		.otherwise({
			redirectTo:'/g'
		});
	});
	
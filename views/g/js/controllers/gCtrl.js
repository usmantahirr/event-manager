firstApp.controller('usersCtrl', function ($scope, gSvc) {
    $scope.persons = gSvc.readData().query();
});
firstApp.controller('userNewCtrl', function ($scope, gSvc, $location) {
    $scope.g_name = null;
    $scope.g_catagory = null;
    $scope.createNew = function () {
        gSvc.readOneData().save({ g_name: $scope.g_name, g_catagory: $scope.g_catagory}, function (data) {
			console.log("created glS:"+$scope.g_name +" g_catagory:"+ $scope.g_catagory);
		});
        $location.path("/g");
    }
});
firstApp.controller('userShowCtrl', function ($scope, gSvc,$routeParams) {
    var newPerson = gSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.person = newPerson;
});
firstApp.controller('userEditCtrl', function ($scope, gSvc, $routeParams,$location) {
    var selectedPerson=gSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.selectedPerson = selectedPerson;
    $scope.saveChanges = function () {
        console.log(selectedPerson.id);
		id=selectedPerson.id;
        gSvc.updateData().update({ id: id }, selectedPerson);
		$location.path("/g");
	}
});
firstApp.controller('userDelCtrl', function ($scope, gSvc, $routeParams,$location) {
	$id = $routeParams.id;
    gSvc.deleteData().delete({ id: $id });
   // $location.path("/gs");
});
firstApp.factory('gSvc', function ($resource) {
    var WebUrl = 'http://localhost:3000/g/';

    return {
        readData: function () {
            return $resource('http://localhost:3000/g/new');
        },
        readOneData: function () {
            return $resource(WebUrl + ':personID');
        },
        updateData: function () {
            return $resource('http://localhost:3000/g/:id', null,
                { 'update': { method: 'PUT' } });
        },
        deleteData: function () {
            return $resource(WebUrl + ':id', null,
                { 'delete': { method: 'DELETE' } });
        }
    }
});
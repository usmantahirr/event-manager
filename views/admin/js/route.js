'use strict';
var firstApp=angular.module("firstApp",['ngRoute', 'ngResource'])
	.config(function ($routeProvider){
		var partialsDir = "admin/partials/admin/";	
		$routeProvider
		.when('/',{
			templateUrl: 'admin/partials/defaultPart.html'
		})
		.when('/admin', {
			templateUrl: partialsDir+'adminPart.html', 
			controller: 'adminCtrl'
		})
		.when('/new', {
			templateUrl: partialsDir+'adminNewPart.html',
			controller: 'adminNewCtrl'
		})
		.when('/:id', {
        templateUrl: partialsDir+'adminShowPart.html',
     	controller: 'adminShowCtrl'
		})
        .when('/:id/edit', {
            templateUrl: partialsDir+'adminEditPart.html',
			controller: 'adminEditCtrl'
        })
        .when('/:id/del', {
            template: '<h1>Deleted</h1>',
			controller: 'adminDelCtrl'
        });
		$routeProvider
		.otherwise({
			redirectTo:'/cat'
		});
	});
	
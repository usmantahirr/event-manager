firstApp.factory('adminSvc', function ($resource) {
    var WebUrl = 'http://localhost:3000/admin/';

    return {
        readData: function () {
            return $resource('http://localhost:3000/admin/new');
        },
        readOneData: function () {
            return $resource(WebUrl + ':personID');
        },
        updateData: function () {
            return $resource('http://localhost:3000/admin/:id', null,
                { 'update': { method: 'PUT' } });
        },
        deleteData: function () {
            return $resource(WebUrl + ':id', null,
                { 'delete': { method: 'DELETE' } });
        }
    }
});
firstApp.controller('adminCtrl', function ($scope, adminSvc) {
    $scope.persons = adminSvc.readData().query();
});
firstApp.controller('adminNewCtrl', function ($scope, adminSvc, $location) {
    //$scope.userName = null;
    $scope.Email = null;
    $scope.password = null;
    $scope.createNew = function () {
        adminSvc.readOneData().save({  email: $scope.email, password: $scope.password }, function (data) {
            console.log("admin created un:"+$scope.userName  +" em:"+ $scope.email +" ps:"+ $scope.password);
        });
        $location.path("/admin");
    }
});
firstApp.controller('adminShowCtrl', function ($scope, adminSvc,$routeParams) {
    var newPerson = adminSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.person = newPerson;
});
firstApp.controller('adminEditCtrl', function ($scope, adminSvc, $routeParams,$location) {
    var selectedPerson = adminSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.selectedPerson = selectedPerson;
    $scope.saveChanges = function () {
        console.log(selectedPerson.id);
        id = selectedPerson.id;
        adminSvc.updateData().update({ id: id }, selectedPerson);
        $location.path("/admin");
    }
});
firstApp.controller('adminDelCtrl', function ($scope, adminSvc, $routeParams,$location) {
    $id = $routeParams.id;
    adminSvc.deleteData().delete({ id: $id });
    $location.path("/admin");
});
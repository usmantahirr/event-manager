firstApp.controller('imageCtrl', function ($scope, imageSvc) {
    $scope.persons = imageSvc.readData().query();
});
firstApp.controller('imageNewCtrl', function ($scope, imageSvc, $location) {
    $scope.img = null;
    $scope.img_title = null;
	$scope.img_des = null;
	$scope.img_order = null;
	$scope.upload=function(){
	$scope.image=$scope.file;
	console.log($scope.file);
	}
    $scope.createNew = function () {
       imageSvc.readOneData().save({ img: $scope.img, img_title: $scope.img_title, img_des: $scope.img_des, img_order: $scope.img_order }, function (data) {
			console.log("image created im:"+ $scope.img  +" ti:"+ $scope.img_title +" ds:"+ $scope.img_des +" or:"+ $scope.img_order);
		});
        $location.path("/image");
    }
});
firstApp.controller('imageShowCtrl', function ($scope, imageSvc,$routeParams) {
    var newPerson = imageSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.person = newPerson;
});
firstApp.controller('imageEditCtrl', function ($scope, imageSvc, $routeParams,$location) {
    var selectedPerson = imageSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.selectedPerson = selectedPerson;
    $scope.saveChanges = function () {
        console.log(selectedPerson.id);
		id = selectedPerson.id;
        imageSvc.updateData().update({ id: id }, selectedPerson);
		$location.path("/image");
	}
});

firstApp.controller('assignVeiwCtrl', function ($scope, imageSvc) {
alert("working image assign");
    $scope.images=imageSvc.assignData().get();
	$scope.assign=function(){alert("Image assigned to Gallery "+$scope.g_id);
	$scope.images=imageSvc.assign_imageData().save({image:$scope.img_id,gallery:$scope.g_id},function(){
	});
	};
 });
 firstApp.controller('assignImageCtrl', function ($scope, imageSvc) {
   
   
   // $scope.images=imageSvc.assign_imageData().save({image:img_id,gallery:g_id},function(){
	//});
 });//data
 
firstApp.controller('imageDelCtrl', function ($scope, imageSvc, $routeParams,$location) {
	$id = $routeParams.id;
    imageSvc.deleteData().delete({ id: $id });
    $location.path("/image");
});
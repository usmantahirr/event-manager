firstApp.factory('imageSvc', function ($resource) {
    var WebUrl = 'http://localhost:3000/image/';

    return {
	
        readData: function () {
            return $resource('http://localhost:3000/image/new');
        },
        readOneData: function () {
            return $resource(WebUrl + ':personID');
        },
        updateData: function () {
            return $resource('http://localhost:3000/image/:id', null,
                { 'update': { method: 'PUT' } });
        },
		assignData: function () {
            return $resource('http://localhost:3000/image/get/assign_view');
        },
		assign_imageData: function () {
            return $resource('http://localhost:3000/image/post/assign_image');
        },
        deleteData: function () {
            return $resource(WebUrl + ':id', null,
                { 'delete': { method: 'DELETE' } });
				
        }
    }
});
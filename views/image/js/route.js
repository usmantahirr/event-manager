'use strict';
var firstApp=angular.module("firstApp",['ngRoute', 'ngResource'])
	.config(function ($routeProvider){
		var partialsDir = "image/partials/image/";	
		$routeProvider
		.when('/',{
			templateUrl: 'image/partials/defaultPart.html'
		})
		.when('/image', {
			templateUrl: partialsDir+'imagePart.html', 
			controller: 'imageCtrl'
		})
		.when('/new', {
			templateUrl: partialsDir+'imageNewPart.html',
			controller: 'imageNewCtrl'
		})
		.when('/upload', {
			templateUrl: partialsDir+ 'uppart.html',
			controller: 'upCtrl'
		})
		.when('/:id', {
        templateUrl: partialsDir+'imageShowPart.html',
     	controller: 'imageShowCtrl'
		})
        .when('/:id/edit', {
            templateUrl: partialsDir+'imageEditPart.html',
			controller: 'imageEditCtrl'
        })
		.when('/:id/assign', {
            templateUrl: partialsDir+'assign.html',
			controller: 'assignVeiwCtrl'
        })
		.when('/:id/assignImage', { 
            templateUrl: partialsDir+'image/assign.html',
			controller: 'assignImageCtrl'
        })
        .when('/:id/del', {
            template: '<h1>Deleted</h1>',
			controller: 'imageDelCtrl'
        });
		$routeProvider
		.otherwise({
			redirectTo:'/image'
		});
	});
	
var app = angular.module('myApp', []);

app.controller('eventsController', function ($scope, $http) {
    $http.get('http://localhost:3000/scheduler/getSchedule').success(function(data) {
        $scope.events = data;
        console.log(data);
    });
});
'use strict';
var firstApp=angular.module("firstApp",['ngRoute', 'ngResource'])
	.config(function ($routeProvider){
		var partialsDir = "video/partials/video/";	
		$routeProvider
		.when('/',{
			templateUrl: 'video/partials/defaultPart.html'
		})
		.when('/video', {
			templateUrl: partialsDir+'videoPart.html', 
			controller: 'videoCtrl'
		})
		.when('/new', {
			templateUrl: partialsDir+'videoNewPart.html',
			controller: 'videoNewCtrl'
		})
		.when('/uploadvideo', {
			templateUrl: partialsDir+ 'uppart.html',
			controller: 'upCtrl'
		})
		.when('/:id', {
        templateUrl: partialsDir+'videoShowPart.html',
     	controller: 'videoShowCtrl'
		})
        .when('/:id/edit', {
            templateUrl: partialsDir+'videoEditPart.html',
			controller: 'videoEditCtrl'
        })
		.when('/:id/assign', {
            templateUrl: partialsDir+'assign.html',
			controller: 'assignVeiwCtrl'
        })
		.when('/:id/assignVideo', { 
            templateUrl: partialsDir+'video/assign.html',
			controller: 'assignVideoCtrl'
        })
        .when('/:id/del', {
            template: '<h1>Deleted</h1>',
			controller: 'videoDelCtrl'
        });
		$routeProvider
		.otherwise({
			redirectTo:'/cat'
		});
	});
	
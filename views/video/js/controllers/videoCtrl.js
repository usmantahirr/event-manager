firstApp.controller('videoCtrl', function ($scope, videoSvc) {
    $scope.persons = videoSvc.readData().query();
});
firstApp.controller('videoNewCtrl', function ($scope, videoSvc, $location) {
    $scope.text = null;
    $scope.title = null;
	$scope.order = null;
	$scope.vid = null;
    $scope.createNew = function () {
        videoSvc.readOneData().save({ text: $scope.text, title: $scope.title, order: $scope.order, vid: $scope.vid }, function (data) {
			console.log("video created tx:"+ $scope.text  +" ti:"+ $scope.title +" or:"+ $scope.order +" vi:"+ $scope.vid);
		});
        $location.path("/video");
    }
});
firstApp.controller('videoShowCtrl', function ($scope, videoSvc,$routeParams) {
    var newPerson = videoSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.person = newPerson;
});
firstApp.controller('videoEditCtrl', function ($scope, videoSvc, $routeParams,$location) {
    var selectedPerson = videoSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.selectedPerson = selectedPerson;
    $scope.saveChanges = function () {
        console.log(selectedPerson.id);
		id = selectedPerson.id;
        videoSvc.updateData().update({ id: id }, selectedPerson);
		$location.path("/video");
	}
});
firstApp.controller('assignVeiwCtrl', function ($scope, videoSvc) {
alert("working video assign");
    $scope.videos=videoSvc.assignData().get();
	$scope.assign=function(){alert("Video assigned to gallery "+$scope.g_id);
	$scope.videos=videoSvc.assign_videoData().save({video:$scope.v_id,gallery:$scope.g_id},function(){
	});
	};
 });
 firstApp.controller('assignVideoCtrl', function ($scope, videoSvc) {
   
   
   
 });
firstApp.controller('videoDelCtrl', function ($scope, videoSvc, $routeParams,$location) {
	$id = $routeParams.id;
    videoSvc.deleteData().delete({ id: $id });
    $location.path("/video");
});
firstApp.factory('videoSvc', function ($resource) {
    var WebUrl = 'http://localhost:3000/video/';

    return {
        readData: function () {
            return $resource('http://localhost:3000/video/new');
        },
        readOneData: function () {
            return $resource(WebUrl + ':personID');
        },
        updateData: function () {
            return $resource('http://localhost:3000/video/:id', null,
                { 'update': { method: 'PUT' } });
		},
		assignData: function () {
            return $resource('http://localhost:3000/video/get/assign_view');
        },
		assign_videoData: function () {
            return $resource('http://localhost:3000/video/post/assign_image');
        },
        deleteData: function () {
            return $resource(WebUrl + ':id', null,
                { 'delete': { method: 'DELETE' } });
        }
    }
});
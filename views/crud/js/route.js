'use strict';
var firstApp=angular.module("firstApp",['ngRoute', 'ngResource'])
	.config(function ($routeProvider){
		var partialsDir = "crud/partials/news/";	
		$routeProvider
		.when('/',{
			templateUrl:'crud/partials/defaultPart.html'
		})
		.when('/news', {
			templateUrl: partialsDir+'newsPart.html', 
			controller: 'newsCtrl'
		})
		.when('/new', {
			templateUrl: partialsDir+'newsNewPart.html',
			controller: 'newsNewCtrl'
		})
		.when('/:id', {
        templateUrl: partialsDir+'newsShowPart.html',
     	controller: 'newsShowCtrl'
		})
        .when('/:id/edit', {
            templateUrl: partialsDir+'newsEditPart.html',
			controller: 'newsEditCtrl'
        })
        .when('/:id/del', {
            template: '<h1>Deleted</h1>',
			controller: 'newsDelCtrl'
        });
		$routeProvider
		.otherwise({
			redirectTo:'/news'
		});
	});
	
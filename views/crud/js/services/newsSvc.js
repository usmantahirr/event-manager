firstApp.factory('crudSvc', function ($resource) {
    var WebUrl = 'http://localhost:3000/crud/';

    return {
        readData: function () {
            return $resource('http://localhost:3000/crud/new');
        },
        readOneData: function () {
            return $resource(WebUrl + ':personID');
        },
        updateData: function () {
            return $resource('http://localhost:3000/crud/:id', null,
                { 'update': { method: 'PUT' } });
        },
        deleteData: function () {
            return $resource(WebUrl + ':id', null,
                { 'delete': { method: 'DELETE' } });
        }
    }
});
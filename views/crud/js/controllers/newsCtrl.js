firstApp.controller('newsCtrl', function ($scope, crudSvc) {
    $scope.persons = crudSvc.readData().query();
});
firstApp.controller('newsNewCtrl', function ($scope, crudSvc, $location) {
    $scope.news_date = null;
    $scope.news_title = null;
	$scope.description = null;
	$scope.news_date = null;
	$scope.news_month = null;
	$scope.news_year = null;
    $scope.createNew = function () {
        crudSvc.readOneData().save({ news_date: $scope.news_date, news_title: $scope.news_title, description: $scope.description,news_day: $scope.news_day,news_month: $scope.news_month,news_year: $scope.news_year}, function (data) {
			console.log("news created fn:"+$scope.news_date +" ln:"+ $scope.news_title +" ds:"+ $scope.description +" ln:"+ $scope.news_day +" ln:"+ $scope.news_month + " ln:"+ $scope.news_year );
		});
        $location.path("/news");
    }
});
firstApp.controller('newsShowCtrl', function ($scope, crudSvc,$routeParams) {
    var newPerson = crudSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.person = newPerson;
});
firstApp.controller('newsArchiveCtrl', function ($scope, crudSvc) {
    $scope.persons = crudSvc.readData().query();
});
firstApp.controller('newsEditCtrl', function ($scope, crudSvc, $routeParams,$location) {
    var selectedPerson = crudSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.selectedPerson = selectedPerson;
    $scope.saveChanges = function () {
        console.log(selectedPerson.id);
		id = selectedPerson.id;
        crudSvc.updateData().update({ id: id }, selectedPerson);
		$location.path("/news");
	}
});
firstApp.controller('newsDelCtrl', function ($scope, crudSvc, $routeParams,$location) {
	$id = $routeParams.id;
    crudSvc.deleteData().delete({ id: $id });
    $location.path("/news");
});
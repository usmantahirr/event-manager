'use strict';
var firstApp=angular.module("firstApp",['ngRoute', 'ngResource'])
	.config(function ($routeProvider){
		var partialsDir = "events/partials/events";	
		$routeProvider
		.when('/',{
			templateUrl:'events/partials/defaultPart.html'
		})
		.when('/events', {
			templateUrl: partialsDir+'eventsPart.html', 
			controller: 'eventsCtrl'
		})
		.when('/new', {
			templateUrl: partialsDir+'eventsNewPart.html',
			controller: 'eventsNewCtrl'
		})
		.when('/:id', {
        templateUrl: partialsDir+'eventsShowPart.html',
     	controller: 'eventsShowCtrl'
		})
        .when('/:id/edit', {
            templateUrl: partialsDir+'eventsEditPart.html',
			controller: 'eventsEditCtrl'
        })
        .when('/:id/del', {
            template: '<h1>Deleted</h1>',
			controller: 'eventsDelCtrl'
        });
		$routeProvider
		.otherwise({
			redirectTo:'/events'
		});
	});
	
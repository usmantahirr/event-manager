firstApp.factory('eventsSvc', function ($resource) {
    var WebUrl = 'http://localhost:3000/events';

    return {
        readData: function () {
            return $resource('http://localhost:3000/events/new');
        },
        readOneData: function () {
            return $resource(WebUrl + ':personID');
        },
        updateData: function () {
            return $resource('http://localhost:3000/events/:id', null,
                { 'update': { method: 'PUT' } });
        },
        deleteData: function () {
            return $resource(WebUrl + ':id', null,
                { 'delete': { method: 'DELETE' } });
        }
    }
});
firstApp.controller('eventsCtrl', function ($scope, eventsSvc) {
    $scope.persons = eventsSvc.readData().query();
});
firstApp.controller('eventsNewCtrl', function ($scope, eventsSvc, $location) {
    $scope.events_date = null;
    $scope.events_title = null;
	$scope.description = null;
    $scope.createNew = function () {
        eventsSvc.readOneData().save({ events_date: $scope.events_date, events_title: $scope.events_title, description: $scope.description }, function (data) {
			console.log("Event created fn:"+$scope.events_date +" ln:"+ $scope.events_title +" ds:"+ $scope.description);
		});
        $location.path("/events");
    }
});
firstApp.controller('eventsShowCtrl', function ($scope, eventsSvc,$routeParams) {
    var newPerson = eventsSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.person = newPerson;
});
firstApp.controller('eventsEditCtrl', function ($scope, eventsSvc, $routeParams,$location) {
    var selectedPerson = eventsSvc.readOneData().get({
        personID: $routeParams.id
    });
    $scope.selectedPerson = selectedPerson;
    $scope.saveChanges = function () {
        console.log(selectedPerson.id);
		id = selectedPerson.id;
        eventsSvc.updateData().update({ id: id }, selectedPerson);
		$location.path("/events");
	}
});
firstApp.controller('eventsDelCtrl', function ($scope, eventsSvc, $routeParams,$location) {
	$id = $routeParams.id;
    eventsSvc.deleteData().delete({ id: $id });
    $location.path("/events");
});